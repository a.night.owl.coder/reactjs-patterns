import React, { Component } from "react";
import user from "./images/user.png";
import "./App.css";
import ButtonGroup from './components/ButtonGroup'

import store from './store/reduxStore'

class App extends Component {
  constructor(props) {
    super(props)
    this.state = store.getState()
  }
  render() {
    const { name, description, tech, location } = store.getState();
    return (
      <div className="App" >
        <section className="User__img">
          <img src={user} alt="user" />
        </section>

        <section className="User__info">
          <p>
            {" "}
            <span className="faint">I am</span> a {description}
          </p>
          <p>
            {" "}
            <span className="faint">I know</span> {tech}
          </p>

          <p className="User__info__details User__info__divider faint">
            <span>Name: </span>
            <span>{name}</span>
          </p>
          <p className="User__info__details faint">
            <span>Based in: </span>
            <span>{location}</span>
          </p>
          <ButtonGroup locationArray={["Shanghai", "Berlin", "London"]} />
        </section>
      </div>
    )
  }
}

export default App;
