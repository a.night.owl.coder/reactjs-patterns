import React from 'react';
import './style.css';
import store from '../store/reduxStore';

function dispatchChangeLocationAction(element) {
    store.dispatch({
        type: "CHANGE_LOCATION",
        location: element.target.dataset.loca
    });
}

const ButtonGroup = ({locationArray}) => (
    <div className="btn-group">
        {locationArray.map((item, i) => 
            <button
                key={`btn-${i}`}
                data-loca={item}
                onClick={dispatchChangeLocationAction}
            >
                {item}
            </button>
        )}
    </div>
)

export default ButtonGroup;