export default (state, action) => {
    switch (action.type) {
        case "CHANGE_LOCATION":
            console.log(action);
            return {
                ...state,
                location: action.location
            }
        default:
            return state
    }
}